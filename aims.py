#!/usr/bin/env python
import numpy as np
def main():
    print "Welcome to the AIMS module"

 
def std(numbers):
    sm=0
    avg= np.average(numbers)
    for i in range(len(numbers)):
        sm = sm + ((numbers[i]-avg)**2)
        
    std= (sm/ len(numbers))**0.5

    return std

def avg_range(file_name):
    result=[]
    new_result= []
    for file in file_name:
        data=open(file)
        for line in data:
            if (line.startswith('Range')):
                result= line.strip().split(':')
                new_result.append(float(result[1]))
        
       
    data.close()
    return (np.average(new_result))
    

if __name__ == "__main__":
    main()

