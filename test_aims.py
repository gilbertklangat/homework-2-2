import aims
from nose.tools import assert_equal
from nose.tools import assert_almost_equal

def test_ints():
    numbers = ([1,2,3,4,5,6])
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert_equal(obs,exp)

def test_negative():
    numbers = ([-1,-2,-3,-4,-5,-6])
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert_equal(obs,exp)

def test_mix():
    numbers = ([-2,-1,0,1,2,3])
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert_equal(obs,exp)

def test_floats():
    numbers = ([1.0,2.0,3.0,4.0,5.0,6.0])
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert_equal(obs,exp)

def test_name():
    filename=['data/bert/audioresult-00215','data/bert/audioresult-00239']
    obs=aims.avg_range(filename)
    exp=6.0
    assert obs==exp

def test_name1():
    filename=['data/bert/audioresult-00466','data/bert/audioresult-00532','data/bert/audioresult-00359']
    obs=aims.avg_range(filename)
    exp=6.0
    assert_almost_equal(obs,exp)

def test_name2():
    filename=['data/bert/audioresult-00330','data/bert/audioresult-00265','data/bert/audioresult-00521','data/bert/audioresult-00451']
    obs=aims.avg_range(filename)
    exp=6.5
    assert_almost_equal(obs,exp)

